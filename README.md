# Multimodal template construction

## Setup

- Create a Conda environment and make sure the latest versions of the following packages are installed. Development and testing
was done with python 3.7. Installation can be performed with:

```bash
conda create -n tempy_env python=3.7 pandas numpy
conda activate tempy_env
conda install -c conda-forge nibabel
conda install -c conda-forge fslpy
pip install file-tree file-tree-fsl
```

- The MMORF singularity package is required for the registrations. It can be downloaded from https://git.fmrib.ox.ac.uk/flange/mmorf_beta.
For the template construction pipeline to find MMORF the environment variable `MMORFDIR` has to be set:
```bash
export MMORFDIR=/path/to/mmorf.sif
```

- It is assumed that FSL is installed.
- Job submission to a computing cluster is currently based on FSL's `fsl_sub` command line tool, 
which assumes that the computing cluster is managed by SGE.

## Running the template construction pipeline
Clone this repository: 
```bash
git clone https://git.fmrib.ox.ac.uk/cart/ms_templates.git
cd ms_templates

python run_template_construction.py [Options]
```

### Options
####   `-h, --help`
Show help message

####   `-i <dir>, --input <dir>`
Absolute path to directory that contains the defaced subjects/timepoints

####   `-t <path>, --tree <path>`
Path to FSL Filetree describing the subject-specific directory structure and selection of modalities.

The input file structure of the data (i.e. can be a timepoint or a subject) has to be defined in the FSL filetree located in `./config/data.tree`.
To construct a combined T1, T2 and DTI template the locations and filenames of e.g. whole-head, brain-extracted and brain masks with respect to 
the timepoint's directory have to be specified. In the template construction code these will be accessed through Filetree keys, 
which must not be altered or removed.
 
Required keys for T1: T1_head, T1_brain, T1_brain_mask  
Required keys for T1+T2: T1_head, T1_brain, T1_brain_mask, T2_head, T2_brain  
Required keys for T1+T2+DTI: T1_head, T1_brain, T1_brain_mask, T2_head, T2_brain, DTI_scalar, DTI_tensor  

For example to construct a combined T1, T2 and DTI template `./config/data.tree` could look like:    

```bash
{sub_id}
    anat
        T1
            T1.nii.gz (T1_head)  
            T1_brain.nii.gz (T1_brain)  
            T1_brain_mask.nii.gz (T1_brain_mask)
        T2
            T2.nii.gz (T2_head)
            T2_brain.nii.gz (T2_brain)
        DTI
            dti_S0.nii.gz (DTI_scalar)
            dti_tensor.nii.gz (DTI_tensor)
```

For a T1-only template this would look like:
```bash
{sub_id}
    anat
        T1
            T1.nii.gz (T1_head)  
            T1_brain.nii.gz (T1_brain)  
            T1_brain_mask.nii.gz (T1_brain_mask)
```

`{sub_id}` is a placeholder for the timepoints directory and will be automatically replaced with:
- IDs in the CSV file provided with `--subids <path>`
- if the `--subids <path>` flag is not provided, the sub-directories of `--input <dir>` will be used

####   `-o <dir>, --output <dir>`
Absolute path to output directory

####   `-s <path>, --subids <path>`
Path to .csv file containing one subject ID per row: subject IDs have to indentify the sub-directories of the 
'input' argument (optional)

If not provided all sub-directories of the --input <dir> argument will be used as subject IDs

####   `-aff [True,False], --affine [True,False]`
Run affine template construction (required for affine template construction)

####   `-nln [True,False], --nonlinear [True,False]`
Run nonlinear template construction (required for nonlinear template construction)

####   `-c <string>, --cpuq <string>`
Name of a cluster queue to submit CPU jobs to (required for affine and nonlinear template construction)

####   `-g <string>, --gpuq <string>`
Name of a cluster queue to submit GPU jobs to (required for nonlinear template construction)

####   `-nres <int>, --n_resolutions <int>`
Number of resolution levels (required for nonlinear template construction)

####   `-nit <int>, --n_iterations <int>`
Number of iterations per resolution level (required for nonlinear template construction)

### Example
A typical command for within-subject template construction given multiple timepoints per subject would lool like this:
```bash
export MMORFDIR=/path/to/mmorf.sif
python run_template_construction.py 
        -i /absolute/path/to/subject/containing/timepoint/directories/
        -t ./config/MS_template.tree 
        -o /absolute/path/to/output/directory/ 
        -aff True 
        -nln True 
        -nres 6 
        -nit 1 
        --cpuq short.qc 
        --gpuq gpu8.q
```

## Output
Output directories will be automatically created for different purposes. These include:
 
####    `log`
Contains log-files from the job submission system; this is very useful for debugging

###     `scripts`
Contains generated Bash and Python scripts e.g. for job array submissions, wrappers for Python functions

####    `affine_it1`
Contains the output of the first set of rigd and affine registrations including 
- rigid registrations between within-subject modalities (i.e. DTI to T2, T2 to T1)
- affine registrations between each subject and a reference subject
- unbiasing transformation removing the bias introduced by the reference subject
- unbiased average T1 template in the unbiased space of the subjects
- rigid transformation of unbiased template to MNI space

####    `affine_it2`






